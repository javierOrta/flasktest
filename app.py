from flask import Flask,jsonify
from flask.json import jsonify
from flask_cors import CORS


app = Flask(__name__)

CORS(app)


tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol',
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn scala',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    },
    {
        'id': 3,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    },
    {
        'id': 4,
        'title': u'Learn java',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    },
    {
        'id': 5,
        'title': u'Learn go',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    }
]


@app.route('/api/tasks', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})


@app.route('/api/tasksp', methods=['POST'])
def post_tasks():
    return jsonify({'tasks': tasks})


@app.route('/api/<nombre>', methods=['POST'])
def post_tasks_nombre(nombre):
    return jsonify({"nombre":nombre})


if __name__ == '__main__':
    app.run(debug=True)
